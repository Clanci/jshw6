// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// екранування стало необхідним через те, що деякі символи компілятор не сприймає правильно. Для правильного сприймання символів в рядку існує запис через бекслеш для правильного його прочитання компілятором

// Які засоби оголошення функцій ви знаєте?
// Function Declaration, Function Expression та Named Function Expression. Function declaration - функція оголошується за допомогою ключового слова function, Function expression - функція також оголошується за допомогою ключового слова function, але вона не має імені, і вона записується в змінну, Named Function expression - функція записується в змінну, а також має своє ім'я (правда функцію не можна буде викликати по цьому імені).

// Що таке hoisting, як він працює для змінних та функцій?
// Hoisting(спливання, підняття) - це механізм, при якому змінні та оголошення функції піднімаються вгору по своїй області видимості перед виконанням коду.

function createNewUser() {
    
    const newUser = {
        firstName: null,
        secondName: null,
        birthday: null,

        get firstName(){
            return this._firstName;
        },
        get secondName(){
            return this._secondName;
        },

        addingFirstName: function () {
            while (true) {
                const newFirstName = prompt("Введіть своє ім'я");
                if (newFirstName !== "" && !/\d/.test(newFirstName) && newFirstName[0] === newFirstName[0].toUpperCase()) {
                    this._firstName = newFirstName;
                    break;
                } else if (newFirstName === "") {
                    alert("Ім'я не має бути порожнім");
                } else if (newFirstName[0] !== newFirstName[0].toUpperCase()) {
                    alert("Ім'я має починатись з великої літери");
                } else if (/\d/.test(newFirstName)) {
                    alert("Ім'я не має містити цифри");
                }
            }
        },

        addingSecondName: function(){
            while (true) {
                const newSecondName = prompt("Введіть своє прізвище");
                if (newSecondName !== "" && !/\d/.test(newSecondName) && newSecondName[0] === newSecondName[0].toUpperCase()) {
                    this._secondName = newSecondName;
                    break;
                } else if (newSecondName === ""){
                    alert("Прізвище не має бути порожнім");
                } else if (/\d/.test(newSecondName)){
                    alert("Прізвище не має містити цифри");
                } else if (newSecondName[0] !== newSecondName[0].toUpperCase()){
                    alert("Прізвище має починатись з великої літери");
                }
            }
        },

        addingBirthday: function() {
            while (true) {
                const inputDate = prompt("Введіть дату свого народження в форматі dd.mm.yyyy");

                const dateRegex = /^\d{2}\.\d{2}\.\d{4}$/;
                if (!dateRegex.test(inputDate)) {
                    alert("Некоректний формат дати. Введіть дату у форматі dd.mm.yyyy");
                    continue;
                }

                const parts = inputDate.split('.');
                const day = parseInt(parts[0], 10);
                const month = parseInt(parts[1], 10) - 1;
                const year = parseInt(parts[2], 10);
                const date = new Date();
                this.birthday = new Date(year, month, day);
                
                if (this.birthday.getFullYear() < 1910 || this.birthday > date || isNaN(this.birthday.getDate())){
                    alert("Некоректна дата народження. Перевірте рік, місяць і день.");
                    continue;
                } else {
                    break;
                }
            }
        },

        getLogin: function() {
            console.log(`Ваш логін: ${this._firstName[0].toLowerCase()}${this._secondName.toLowerCase()}`);
        },

        getAge: function(){
            const currentDate = new Date();
            const currentYear = currentDate.getFullYear();
            const age = currentYear - this.birthday.getFullYear();
            console.log(`вік користувача: ${age}`);
            return age;
        },

        getPassword: function(){
            console.log(`Пароль: ${this._firstName[0]}${this._secondName.toLowerCase()}${this.birthday.getFullYear()}`);
        }
    }

    newUser.addingFirstName();
    newUser.addingSecondName();
    newUser.addingBirthday();
    newUser.getLogin();
    newUser.getAge();
    newUser.getPassword();

    for (let key in newUser) {
        if (key === 'firstName' || key === 'secondName' || key === 'birthday'){
            document.write(`<br><strong>${key}:</strong> ${newUser[key]}<br>`);
        }
    }
}

createNewUser();

